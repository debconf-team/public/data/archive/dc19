# DebConf19 Design Team

All artwork in this repository has been produced collaboratively and is
licensed in a way that allows such reproduction.

## Authors:

* Angelo Rosa
* Jefferson Maier
* Mulin,
* Valéssio Brito

## Contribuition/revision:

* Adriana Costa
* Antonio Terceiro
* Daniel Lenharo
* Giovani Ferreira
* Lucas Kanashiro
* Paulo Henrique Santana
* Samuel Henrique

## License:

[Creative Commons Attribution Share Alike 4.0](https://salsa.debian.org/debconf-team/public/data/dc19/blob/master/LICENSE.txt)
