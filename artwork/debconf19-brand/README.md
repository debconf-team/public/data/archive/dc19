# DebConf19 Repository

## Logo

A ["Tamanduá-bandeira"](https://en.wikipedia.org/wiki/Giant_anteater) (A brazilian Giant anteater specie) with a spiral tongue, wich refers to Debian. Besides that, the spiral carries the nº9 format, complementing the typography "DebConf19". The "Tamanduá-bandeira" is one of the 4 bigger species of anteaters finded on Central America and South America, and is a species in danger of extintion. It's easily recognized for the long nose and extended tongue, this caracteristics facilitate the diet of ants and termites. The animal is a brazilian symbol.

Created by Mulin

The license of the logo is [Creative Commons Attribution-ShareAlike 4.0 Unported](https://creativecommons.org/licenses/by-sa/4.0/). 