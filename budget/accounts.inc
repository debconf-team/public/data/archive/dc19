account assets:cash
account assets:debian-france
account assets:ICTL
account assets:SPI

account expenses:bursaries:general bursaries
account expenses:bursaries:diversity bursaries
account expenses:bursaries:travel
account expenses:child care
account expenses:content
account expenses:daytrip
account expenses:fees
account expenses:general
account expenses:graphic materials:banner
account expenses:graphic materials:paper
account expenses:graphic materials:poster
account expenses:incidentals
account expenses:incidentals:bank
account expenses:incidentals:frontdesk
account expenses:incidentals:misc
account expenses:incidentals:transport
account expenses:infra
account expenses:insurance
account expenses:local team:computer
account expenses:local team:food
account expenses:local team:transportation
account expenses:party:cheese and wine
account expenses:party:conference dinner:food
account expenses:party:conference dinner:drink
account expenses:party:conference dinner:bus
account expenses:postal
account expenses:press
account expenses:roomboard:accommodation:access point
account expenses:roomboard:accommodation:bedrooms
account expenses:roomboard:accommodation:cleaning
account expenses:roomboard:accommodation:coffee and water
account expenses:roomboard:accommodation:internet link
account expenses:roomboard:accommodation:vegan food
account expenses:roomboard:bar beer
account expenses:roomboard:food:catering:barbecue
account expenses:roomboard:food:catering:daytrip
account expenses:roomboard:food:catering:ru
account expenses:roomboard:food:catering:hotel
account expenses:roomboard:food:catering:barbecue
account expenses:roomboard:food:coffee and tea
account expenses:roomboard:food:coffee machine
account expenses:roomboard:food:break-fast
account expenses:roomboard:food:food
account expenses:sponsors
account expenses:swag:backpack
account expenses:swag:badge paper
account expenses:swag:boton
account expenses:swag:drink cup
account expenses:swag:havaianas
account expenses:swag:lanyard
account expenses:swag:tag
account expenses:swag:t-shirt
account expenses:tax
account expenses:travel costs for invited speaker
account expenses:venue:rent
account expenses:venue:staff
account expenses:video:cable
account expenses:video:computer rental
account expenses:video:fiber
account expenses:video:general
account expenses:video:insurence
account expenses:video:projector
account expenses:video:sound equipment
account expenses:visa
account incomes:registration
account incomes:registration:paypal for SPI
account incomes:registration:cash for ICTL
account incomes:daytrip
account incomes:daytrip:cash for ICTL
account incomes:general
account incomes:general:cash for ICTL
account incomes:accommodation
account incomes:bar
account incomes:bar:cash for ICTL
account incomes:bar:credit card for ICTL
account incomes:sponsors:national:bronze
account incomes:sponsors:international:bronze
account incomes:sponsors:national:gold
account incomes:sponsors:international:gold
account incomes:sponsors:national:platinum
account incomes:sponsors:international:platinum
account incomes:sponsors:national:silver
account incomes:sponsors:international:silver
account incomes:sponsors:national:supporter
account incomes:sponsors:international:supporter
account incomes:sponsors:national:donations
account incomes:sponsors:international:donations

account liabilities:helen
account liabilities:phls
account liabilities:lenharo
account liabilities:stefanor
account liabilities:holger
account liabilities:terceiro
